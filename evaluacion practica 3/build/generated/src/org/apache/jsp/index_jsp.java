package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;
import java.sql.PreparedStatement;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("        <link href=\"css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\"/> \r\n");
      out.write("        <title>JSP Page</title>\r\n");
      out.write("    </head>\r\n");
      out.write("    <body style=\"margin-top: 12px; background: rgb(85, 48 ,93)\">      \r\n");
      out.write("        ");


        Connection cone;
            String url = "jdbc:mysql://localhost:3306/recurso_humano";
            String Driver = "com.mysql.jdbc.Driver";
            String user = "root";
            String clave = "";
            Class.forName(Driver);
            cone = DriverManager.getConnection(url, user, clave);
            //Emnpezamos Listando los Datos de la Tabla Usuario
            Statement smt;
            ResultSet rs;
            smt = cone.createStatement();
            rs = smt.executeQuery("select * from tb_persona");
            //Creamo la Tabla:     
        
      out.write("\r\n");
      out.write("       \r\n");
      out.write("        <br>               \r\n");
      out.write("        <div class=\"container\" >        \r\n");
      out.write("            <a href=\"agregar_registros.jsp\" class=\"btn btn-success btn-lg\" >Nuevo Registro </a>\r\n");
      out.write("            <table class=\"table table-responsive\"  id=\"tablaDatos\" style=\"margin-top: 30px\">\r\n");
      out.write("                    <thead>\r\n");
      out.write("                        <tr style=\"background: #a6e1ec\">\r\n");
      out.write("                            <th class=\"text-center\">ID</th>\r\n");
      out.write("                            <th class=\"text-center\">DUI</th>\r\n");
      out.write("                            <th class=\"text-center\">Apellidos</th>\r\n");
      out.write("                            <th class=\"text-center\">Nombres</th>\r\n");
      out.write("                            <th class=\"text-center\">Acciones</th>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                    </thead>\r\n");
      out.write("                    <tbody id=\"tbodys\">\r\n");
      out.write("                        ");

                            while (rs.next()) {
                        
      out.write("\r\n");
      out.write("                        <tr style=\"background: yellowgreen\">\r\n");
      out.write("                            <td class=\"text-center\" style=\" width:  115px\">");
      out.print( rs.getInt("id"));
      out.write("</td>\r\n");
      out.write("                            <td class=\"text-center\" style=\" width:  215px\">");
      out.print( rs.getString("dui_persona"));
      out.write("</td>\r\n");
      out.write("                            <td class=\"text-center\" style=\" width:  215px\">");
      out.print( rs.getString("apellidos_persona"));
      out.write("</td>\r\n");
      out.write("                            <td class=\"text-center\" style=\" width:  215px\">");
      out.print( rs.getString("nombre_persona"));
      out.write("</td>\r\n");
      out.write("                            <td class=\"text-center\" style=\" width:  175px\">\r\n");
      out.write("                                \r\n");
      out.write("                                \r\n");
      out.write("                                <a href=\"Modificar.jsp?id=");
      out.print( rs.getInt("id"));
      out.write("\" class=\"btn btn-primary\">Modificar</a>\r\n");
      out.write("                                <a href=\"Delete.jsp?id=");
      out.print( rs.getInt("id"));
      out.write("\" class=\"btn btn-danger\">Delete</a>\r\n");
      out.write("                            </td>\r\n");
      out.write("                        </tr>\r\n");
      out.write("                        ");
}
      out.write("\r\n");
      out.write("                </table>\r\n");
      out.write("            </div>        \r\n");
      out.write("        \r\n");
      out.write("        </div>        \r\n");
      out.write("        <script src=\"js/jquery.js\" type=\"text/javascript\"></script>             \r\n");
      out.write("        <script src=\"js/bootstrap.min.js\" type=\"text/javascript\"></script>        \r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
