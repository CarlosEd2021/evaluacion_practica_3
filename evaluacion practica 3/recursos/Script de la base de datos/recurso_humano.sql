-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-06-2021 a las 23:19:55
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `recurso_humano`
--
CREATE DATABASE IF NOT EXISTS `recurso_humano` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `recurso_humano`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_persona`
--

CREATE TABLE IF NOT EXISTS `tb_persona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dui_persona` varchar(30) NOT NULL,
  `apellidos_persona` varchar(30) NOT NULL,
  `nombre_persona` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tb_persona`
--

INSERT INTO `tb_persona` (`id`, `dui_persona`, `apellidos_persona`, `nombre_persona`) VALUES
(10, '12341234-1', 'Valle Pineda', 'Angel Daniel'),
(11, '12341234-2', 'Garcia Alvarez', 'Cristian Eli'),
(12, '12341234-3', 'Del cid Gaitán', 'Carlos Eduardo'),
(13, '12341234-4', 'Grande Pineda', 'Leonardo Misael'),
(14, '12341234-5', 'Bonilla Chávez', 'Andreina Isabel');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
