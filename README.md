El proyecto consiste en la creación de un CRUD para almacenar registros en una tabla de tb_categoria,tb_producto y tb_usuario de la base de datos llamada bd_invetario.

El proyecto se ha desarrollado utilizando el IDE de NeatBeans con Java, MySQL y JDK. 


NOMBRE DE ESTUDIANTES:  	 CARNET           GRUPO           ROL

Ángel Daniel Valle Pineda        212920          SIS21A           Back-end
Cristian Elí García Alvares 	 217620          SIS21B           Analista
Carlos Eduardo Del Cid Gaitán    218220          SIS21B           Scrum Master
Leonardo Misael Grande Pineda  	 212820          SIS21A           Front-end
Andreina Isabel Bonilla Chávez   210820          SIS21B           Diseñador BD

N° GRUPO:
Development Team #6

NOMBRE DEL DOCENTE:
Manuel de Jesús Gámez López           (Product Owner)